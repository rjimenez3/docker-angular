# BUILD AND TAG DOCKER IMAGE
docker build -f "Dockerfile-dev" -t "angular:dev" --no-cache .

# RUN CONTAINER
docker run -v ${PWD}:/app -v /app/node_modules -p 4201:4200 --rm angular:dev


# FOR CI
docker build -f "Dockerfile-dev" -t "angular:app" .
docker run --name "angularapp" --rm -d "angular:app"
docker cp angularapp:app/junit-TEST.xml $(System.DefaultWorkingDirectory)
docker cp angularapp:app/coverage $(System.DefaultWorkingDirectory)
docker stop angularapp